# Instructions to execute

This repository comes with a file to test .Feel free to provide another input
Command usage : php solution.php -f{path to input} -r{number of top results} 
Command returns an array with an index named bigger that contains bigger results and
an index named failed that contains lines with non integer values

# Tasks response

Quicksort algorithm complexity worst case is O(n2)
Quicksort algorithm complexity best case is O(nLogn)

Sure, there is room for improvement as this solution was made with copy-paste driven develoment. I would like to point out that my solution correspond to my limited knowledge of sorting algorithms.