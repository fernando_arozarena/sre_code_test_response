<?php

$failed = array();

function quicksort($array)
{
    global $failed;
    $length = count($array);
    if ($length <= 1)  {        
        if(!empty($array) && preg_match("/(.*?)[a-zA-Z].*/", $array[$length-1]) ) {
            array_push($failed, $array[$length-1]);
            unset($array[$length-1]);
        }
        return $array;
    } else {
        if(preg_match("/(.*?)[a-zA-Z].*/", $array[$length - 1])) {
            array_push($failed, $array[$length - 1]);
            unset($array[$length - 1]);
            return quicksort($array);
        }
        $pivot = $array[$length - 1];
        $left = array();
        $right = array();
        for ($i = 0; $i < $length - 1; $i++) {
            if ($array[$i] < $pivot) {
                $left[] = $array[$i];
            } else {
                $right[] = $array[$i];
            }
        }
        return array_merge(quicksort($left), array($pivot), quicksort($right));
    }
}

$filename = getopt("f:");
$resultsChunk = getopt("r:");

if (!$filename || !$resultsChunk) {
    echo "Must provide a filename and a number of results to return";
    echo "Command usage php solution.php -f{filename_path} -r{number of results}";
    return;
}

if (!file_exists($filename['f'])){
    echo "ERROR: input file does not exist";
    return;
}

$a = file_get_contents($filename['f']);

if ($a === FALSE) {
    echo "input file is not readable or empty";
    return;
} 

if (preg_match("/(.*?)[a-zA-Z].*/", $resultsChunk['r'])){
    echo "sasasasasa";
    return;
}

if (intval($resultsChunk['r']) <= 0 || intval($resultsChunk['r']) >30000000){
    echo "Number of results must be between 0 and 30000000";
    return;
}

$a = explode(PHP_EOL, $a);

$response = []; 
$result = quicksort($a);
$resultsChunk = intval($resultsChunk['r']);

print_r($response = array(
    "bigger" => array_slice($result, -$resultsChunk),   
    "failed" => $failed
));
